const express = require(`express`);
const mongoose = require(`mongoose`);
const app = express();
const port = 3000;

// [SECTION] MongoDB connection
/*
	mongoose.connect("<MongoDB connection string", {useNewUrlParser: true})
*/
// Connecting to MongoDB atlas
// Add password and database name
mongoose.connect("mongodb+srv://admin123:admin123@cluster0.eua2rv1.mongodb.net/s35?retryWrites=true&w=majority", {
	// Due to update in MongoDB drivers that allow connection to it, the default connection is being flagged as error
	// Allows us to avoid any current and future errors while connecting to MongoDB Atlas
	useNewUrlParser: true,
	useUnifiedTopology: true
});

// Connecting to MongoDB locally
let db = mongoose.connection;
// If a connection error occured, print the output in terminal
db.on("error", console.error.bind(console, "connection error"));
// If the connection is successful, print the message "Connected to MongoDB Atlas"
db.once("open", () => console.log("Connected to MongoDB Atlas"));

app.use(express.json());
app.use(express.urlencoded({extended: true}));

// [SECTION] Mongoose Schema

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
});


// [SECTION] Models
// Models must be in singular form and capitalized
// first parameter - indicates the collection in where to store data
// Second parameter - specify the schema/blueprint of the documents that will be stored in the MongoDB collection
const Task = mongoose.model("Task", taskSchema);


// Creating a new Task
/*
Business Logic
 Add a functionality to check if there are duplicate tasks
- If the task already exists in the database, we return an error
- If the task doesn't exist in the database, we add it in the database
The task data will be coming from the request's body
Create a new Task object with a "name" field/property
The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/


app.post("/tasks", (req, res) => {
	// To check if there are duplicate tasks
	Task.findOne({name: req.body.name}, (err, result) => {
		if(result != null && result.name == req.body.name){
			return res.send("Duplicate task found!");
		} else {
			let newTask = new Task({
				name: req.body.name
			});
		newTask.save((saveErr, savedTask) => {
		if(saveErr){
			return console.error(saveErr);
		} else {
			return res.status(201).send("New task created");			};
		})
		};
	});
});


// Get all the tasks
/*
Business Logic
1. Retrieve all the documents
2. If an error is encountered, print the error
3. If no errors are found, send a success status back to the client/Postman and return an array of documents
*/

app.get("/tasks", (req, res) => {
	Task.find({}, (err, result) => {
		if(err) {
			return (console.log(err));
		} else {
			return res.status(200).json({
				data: result
			})
		}
	})
});


app.listen(port, () => console.log(`Server is running at ${port}.`));