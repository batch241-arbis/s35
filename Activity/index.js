const express = require(`express`);
const mongoose = require(`mongoose`);
const app = express();
const port = 3000;

mongoose.connect("mongodb+srv://admin123:admin123@cluster0.eua2rv1.mongodb.net/s35?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("Connected to MongoDB Atlas"));

app.use(express.json());
app.use(express.urlencoded({extended: true}));

const userSchema = new mongoose.Schema({
	username: String,
	password: String
});

const User = mongoose.model("User", userSchema);

app.post("/signup", (req, res) => {
	User.findOne({username: req.body.username}, (err, result) => {
		if(result != null && result.username == req.body.username){
			return res.send("User is already registered");
		} else {
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			});
			newUser.save((saveErr, savedUser) => {
			if(saveErr){
			return console.error(saveErr);
			} else {
			return res.status(201).send("User is now registered");			
				};
			})
		};
	});
});


app.listen(port, () => console.log(`Server is running at ${port}.`));